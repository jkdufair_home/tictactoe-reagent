(ns ^:figwheel-hooks tictactoe-reagent.core
  (:require
   [goog.dom :as gdom]
   [reagent.core :as r]
   [reagent.dom :as rdom]
   [tictactoe-reagent.logic :as logic :refer [best-move make-board next-marker random-move rows terminal-state? width winner]]
	 [cljs.pprint :refer [pprint]]
	 [peerjs]))

;; define your app data so that it doesn't get over-written on reload
(defonce app-state (r/atom nil))
(defonce is-loading (r/atom false))

(defn get-app-element []
  (gdom/getElement "app"))

(defn loading []
	(when @is-loading
		[:div {:style
					 {:width "100%"
						:text-align "center"}}
		 [:p "thinking"]]))

(defn make-move [position]
	;; Make human move regardless
	(swap! app-state #(assoc @app-state :board (logic/make-move (:board @app-state) position)))
	;; Make AI move if opponent is AI
  (when (and
				 (= (:opponent @app-state) :ai)
				 (not (terminal-state? (:board @app-state))))
		(reset! is-loading true)
		(js/setTimeout
		 (fn []
			 (let [best-move (best-move (:board @app-state) 1)]
				 (swap! app-state #(assoc @app-state :board (logic/make-move (:board @app-state) best-move))))
			 (reset! is-loading false))
		 50)))

(defn game-board []
  (let [winner (winner (:board @app-state))]
		[:<>
		 [:div {:style
						{:width "100%"
						 :text-align "center"}}
			[:table {:style	{:border-collapse "collapse"
											 :margin "0 auto 3rem"}}
			 [:tbody
				(doall
				 (map-indexed (fn [row-index row]
												[:tr
												 {:key row-index}
												 (doall
													(map-indexed (fn [column-index cell]
																				 [:td {:id				(str (+ (* (width (:board @app-state)) row-index) column-index))
																							 :key			column-index
																							 :style		{:border-left (if (> column-index 0) "3px solid black" "none")
																												 :border-top  (if (> row-index 0) "3px solid black" "none")
																												 :width       "110px"
																												 :height      "110px"
																												 :padding     "10px"}
																							 :onClick	(fn [e]
																													(or winner
																															cell
																															(let [position	(-> e .-currentTarget .-id)]
																																(make-move (int position)))))}
																					(and cell
																							 [:img {:src		(cond
																																(= cell "X")	"img/x.png"
																																(= cell "O")	"img/o.png")
																											:style {:width "100px"}}])])
																			 row))])
											(rows (:board @app-state))))]]
			[:<>
       (if winner
         [:div (str winner " won!")]
         (if (terminal-state? (:board @app-state))
           [:div "Cat's game!"]
           [:div (str (next-marker (:board @app-state)) "'s turn")]))
       (when (terminal-state? (:board @app-state))
         [:button {:onClick	(fn [] (reset! app-state nil))} "New Game"])]]]))

(defn game-start []
	(reagent.core/with-let [board-size (r/atom 3)]
		(let [parsed-board-size (if (= @board-size "") 3 @board-size)]
			[:div {:style
						 {:width "100%"
							:text-align "center"}}
			 [:p "Would you like to play a game?"]
			 [:button {:onClick	(fn [] (reset! app-state {:board	(make-board parsed-board-size)	:opponent :human}))} "Human"]
			 [:button {:onClick	(fn [] (reset! app-state {:board	(make-board parsed-board-size)	:opponent :ai}))} "AI"]
			 [:div
				[:p "Board size:"]
				[:input {:type "number" :value @board-size
								 :onChange (fn [e] (reset! board-size (-> e .-currentTarget .-value)))}]]])))

(defn game []
	(if (nil? @app-state)
		[game-start]
		[:div
		 [game-board]
		 [loading]]))

(defn mount [el]
  (rdom/render [game] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element))
