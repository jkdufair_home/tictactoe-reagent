(ns tictactoe-reagent.logic
	  (:require
		 [cljs.pprint :refer [pprint]]))

;; Assumptions
;; X plays first
;; AI is O
;; Player as int: X is 1, AI is -1

(def markers ["X" "O"])

(defn make-board [size]
  (vec (take (* size size) (repeat nil))))

(defn width [board]
  (int (Math/sqrt (count board))))

(defn rows [board]
  (let [board-width (width board)]
    (partition board-width board)))

(defn columns [board]
  (apply map vector (rows board)))

(defn diagonals [board]
  (let [skip-forward-slash (dec (width board))
        skip-backslash (inc (width board))]
    (list
     ;; forward slash diagonal
     (as-> skip-forward-slash $
       (drop $ board)
       (drop-last skip-forward-slash $)
       (take-nth skip-forward-slash $))
     ;; backslash diagonal
     (take-nth skip-backslash board))))

(defn next-marker [board]
  (if (even? (count (filter identity board)))
    "X" "O"))


(defn valid-move? [board position]
  (nil? (nth board position)))

(defn valid-moves [board]
  (map first
       (filter #(= (second %) nil) (map-indexed vector board))))

(defn make-move
  [board position]
  (when (valid-move? board position)
    (assoc board position (next-marker board))))

(defn random-move [board]
  (rand-nth (valid-moves board)))

(defn all-equal-marker [marker marked-positions]
  (and (every? #(= marker %) marked-positions)
       marker))

(defn marked-positions-winner [marked-positions]
  (some identity (map #(all-equal-marker % marked-positions) markers)))

(defn winner [board]
	"Which marker has won the game for BOARD or nil if neither has won."
  (some identity (map #(marked-positions-winner %)
                      (concat
                       (rows board)
                       (columns board)
                       (diagonals board)))))

(defn terminal-state? [board]
	(or (empty? (valid-moves board))
			(winner board)))

(defn player-from-marker [marker]
	"-1 if MARKER is \"X\" (human), 1 if MARKER is \"O\" (AI)"
	(if (= marker "X") -1 1))

(defn marker-from-player [player]
	(if (= player -1) "X" "O"))

(defn most-consecutive [s player]
	(apply max
				 (map count
							(filter
							 #(= (marker-from-player player) (first %))
							 (partition-by identity s)))))

(defn score [board player]
	"Score for a BOARD. ##Inf means PLAYER wins. ##-Inf means PLAYER loses. 0 means tie. Otherwise a
	heuristic is applied where more consecutive markers moves the score more toward one of the
	infinities."
	(let [winner (winner board)]
		(cond (nil? winner) (most-consecutive board player)
					(= (player-from-marker winner) player) ##Inf
					(= (player-from-marker winner) (* player -1)) ##-Inf)))

(defn minimax
  "Minimax algorithm with alpha-beta pruning and depth limiting"
	([board player]
	 (minimax board player 0))
	([board player level]
	 (if (or (terminal-state? board)
					 (> level (let [board-width (width board)]
											(cond (<= board-width 3) 4
														(= board-width 4) 2
														:else 1))))
		 ;; base case, return score for that depth with depth being a function of board size and
		 ;; permissible calculation time, maintaining AI success
		 (score board player)
		 ;; iterate over remaining moves, scoring according to minimax algorithm
		 (reduce
			(fn [prev-score indexed-move]
				(let* [next-board (make-move board (first indexed-move))
							 ;; Other player's turn. Score their turn, but as opposite
							 next-score (* (minimax next-board (* player -1) (inc level)) -1)]
					(if (> next-score prev-score)
						(if (= next-score ##Inf)
							(reduced next-score)
							next-score)
						prev-score)))
			##-Inf
			(zipmap (valid-moves board) (range))))))

(def minimax-memo (memoize minimax))

(defn next-boards-and-moves [board]
	(map (fn [move] {:move move :board (make-move board move)}) (valid-moves board)))

(defn best-move [board player]
	(:move
	 (:board-and-move
		(first
		 (sort
			(fn [bam1 bam2] (< (:score bam1) (:score bam2)))
			(map (fn [bam] {:score (minimax-memo (:board bam) (* player -1)) :board-and-move bam}) (next-boards-and-moves board)))))))
